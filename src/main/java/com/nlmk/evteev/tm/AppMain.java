package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.dao.ProjectDAO;
import com.nlmk.evteev.tm.dao.TaskDAO;

import java.util.Scanner;

import static com.nlmk.evteev.tm.util.TerminalConst.*;

/**
 * Основной класс
 */
public class AppMain {

    private static final ProjectDAO projectDAO = new ProjectDAO();
    private static final TaskDAO taskDAO = new TaskDAO();
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        displayWelcome();
        run(args);
        String command = "";
        while (!CMD_EXIT.equals(command))
        {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    /**
     * Обработка консольного ввода
     *
     * @param cmdString команда с консоли
     * @return код выполнения
     */
    private static int run(final String cmdString) {
        if (cmdString == null) return -1;
        switch (cmdString)
        {
            case CMD_VERSION:
                return displayVersion();
            case CMD_HELP:
                return displayHelp();
            case CMD_ABOUT:
                return displayAbout();
            case CMD_EXIT:
                displayExit();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();

            default:
                return displayError();
        }
    }

    /**
     * Показ сообщения об ошибке
     */
    private static int displayError() {
        System.out.println("Неподдерживаемый аргумент. наберите команду " +
            "help для получения списка доступных команд...");
        return -1;
    }

    /**
     * Показ сведений о ключах
     */
    private static int displayHelp() {
        System.out.println("version - Информация о версии.");
        System.out.println("about - Информация о разработчике.");
        System.out.println("help - Информация о доступных командах.");
        System.out.println("exit - Завершение работы приложения");
        System.out.println("proj-create - Создание проекта");
        System.out.println("proj-clear - Очистка проектов");
        System.out.println("proj-list - Вывод списка проектов");
        System.out.println("task-create - Создание задачи");
        System.out.println("task-clear - Очистка задачи");
        System.out.println("task-list - Вывод списка задач");
        return 0;
    }

    /**
     * Показ сведений о версиях
     */
    private static int displayVersion() {
        System.out.println("1.0.5");
        return 0;
    }

    /**
     * Показ сведений об авторе
     */
    private static int displayAbout() {
        System.out.println("Author: Sergey Evteev");
        System.out.println("e-mail: sergey@evteev.ru");
        return 0;
    }

    /**
     * Показ приветствия
     */
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Стандартный выход
     */
    private static void displayExit() {
        System.out.println("Получена команда завершения работы...");
        System.exit(0);
    }

    /**
     * Создание проекта
     *
     * @return код выполнения
     */
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Укажите имя проекта: ");
        final String lv_name = scanner.nextLine();
        projectDAO.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка проекта
     *
     * @return код выполнения
     */
    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список проектов
     *
     * @return код выполнения
     */
    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Создание задачи
     *
     * @return код выполнения
     */
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Укажите имя задачи: ");
        final String lv_name = scanner.nextLine();
        taskDAO.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка задачи
     *
     * @return код выполнения
     */
    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список задач
     *
     * @return код выполнения
     */
    private static int listTask() {
        System.out.println("[LIST PROJECT]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }


}
