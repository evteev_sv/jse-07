package com.nlmk.evteev.tm.util;

/**
 * Класс, содержит терминальные константы
 */
public class TerminalConst {

    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";
    public static final String PROJECT_CREATE = "proj-create";
    public static final String PROJECT_CLEAR = "proj-clear";
    public static final String PROJECT_LIST = "proj-list";
    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";

}
