package com.nlmk.evteev.tm.dao;

import com.nlmk.evteev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с объектом задачи
 * {@link com.nlmk.evteev.tm.entity.Task}
 */
public class TaskDAO {

    private List<Task> tasks = new ArrayList<>();

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param p_name имя задачи
     * @return объект типа {@link com.nlmk.evteev.tm.entity.Task}
     */
    public Task create(final String p_name) {
        final Task task = new Task(p_name);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка списка задач
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link java.util.List}
     */
    public List<Task> findAll() {
        return tasks;
    }

}
