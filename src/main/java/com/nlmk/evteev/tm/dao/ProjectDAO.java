package com.nlmk.evteev.tm.dao;

import com.nlmk.evteev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с объектом проект
 * {@link com.nlmk.evteev.tm.entity.Project}
 */
public class ProjectDAO {

    private List<Project> projects = new ArrayList<>();

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param p_name имя проекта
     * @return объект типа {@link com.nlmk.evteev.tm.entity.Project}
     */
    public Project create(final String p_name) {
        final Project project = new Project(p_name);
        projects.add(project);
        return project;
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link java.util.List}
     */
    public List<Project> findAll() {
        return projects;
    }

}
