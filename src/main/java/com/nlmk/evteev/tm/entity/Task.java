package com.nlmk.evteev.tm.entity;

/**
 * Класс, описывающий задачи
 */
public class Task {

    //<editor-fold defaultstate="collapsed" desc="Объявление переменных">
    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    //</editor-fold>

    /**
     * Конструктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор класса
     *
     * @param p_name название задачи
     */
    public Task(String p_name) {
        name = p_name;
    }

    //<editor-fold defaultstate="collapsed" desc="Методы установки/получения полей">
    public Long getId() {
        return id;
    }

    public void setId(Long p_id) {
        id = p_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String p_name) {
        name = p_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String p_description) {
        description = p_description;
    }
    //</editor-fold>

    @Override
    public String toString() {
        return "Task {id=" + id + ", name=" + name + "}";
    }

}
