package com.nlmk.evteev.tm.entity;

/**
 * Класс, описывающий проект
 */
public class Project {

    //<editor-fold defaultstate="collapsed" desc="Объявление переменных">
    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    //</editor-fold>


    /**
     * Конструктор по умолчанию
     */
    public Project() {
    }

    /**
     * Создание класса
     *
     * @param p_name имя проекта
     */
    public Project(String p_name) {
        name = p_name;
    }

    //<editor-fold defaultstate="collapsed" desc="Методы получения/Присвоения полей">
    public Long getId() {
        return id;
    }

    public void setId(Long p_id) {
        id = p_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String p_name) {
        name = p_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String p_description) {
        description = p_description;
    }
    //</editor-fold>

    @Override
    public String toString() {
        return "Project {id=" + id + ", name=" + name + "}";
    }

}
